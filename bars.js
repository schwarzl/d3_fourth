{
// svg settings
let w = 600;
var h = 250;
var padding    = 30;
let barPadding =  2;

// rng settings
let numBars = 25;
let maxVal  = 25;
let minVal  =  0;

// random number generation
function generateRandom(num, maxVal, minVal) {
    var randomDataset = [];
    for (let i = 0; i < num; i++) {
        randomDataset.push(Math.random() * (maxVal-minVal) + minVal);
    }
    return randomDataset;
}

// populate randomDataset
var randomDataset = generateRandom(numBars, maxVal, minVal); 

// create ordinal scale for positions on x-axis
var xScale = d3.scaleBand()
             .range([padding, w - padding])
             .domain(d3.range(randomDataset.length))
             .paddingInner(0.05);

// and linear scale for bar heights
var yScale = d3.scaleLinear()
           .range([h - padding, padding])
           .domain([0, d3.max(randomDataset)])
           .nice();

// and linear scale for bar color
var cScale = d3.scaleLinear()
            .range([0, 255])
            .domain([d3.min(randomDataset),
                     d3.max(randomDataset)]);
    
// svgB will hold our bar plot
let svgB = d3.select("#svgBar")
    .append("svg")
    .attr("viewBox", "0, 0, " + w + ", " + h);
    // SVG internal coordinate system is defined in viewBox
    // can be used to calculate component sizes
svgB.attr("width", "100%");
    // to allow scaling width is set to 100% - relative to the enclosing div
    // height is not set (default is auto) to keep the proper aspect ratio
    // apparently setting it to "auto" from D3 doesn't work, it wants a number
 
// Adding the bars
let bars = svgB.selectAll("rect")
               .data(randomDataset)
               .enter()
               .append("rect")
               .attr("y", function(d) {
                    return yScale(d);
               })
               .attr("x", function(d, i) {
                    return xScale(i);
               })
               .attr("width", xScale.bandwidth)
               .attr("height", function(d) {
                    return yScale.range()[0] - yScale(d);
               })
               .on("click", function(d) {
                    alert(d);
               })
               .on("mouseover", function(d) { 
                    d3.select(this)
                       .transition("mouseover")
                       .duration(200)
                      .attr("fill", "orange");

                    var xPosition = parseFloat(d3.select(this).attr("x")) + 
                                  xScale.bandwidth() / 2;
                    var yPosition = parseFloat(d3.select(this).attr("y")) + 14;
                    var yMiddlePosition = parseFloat(d3.select(this).attr("y")) / 2 + h / 2 ;

                    d3.select("#tooltipDiv")
                        .style("left", xPosition + "px")
                        .style("top", yMiddlePosition + "px")
                        .select("#value")
                        .text(d.toFixed(3));

                    d3.select("#tooltipDiv").classed("hidden", false);

                    svgB.append("text")
                        .attr("id", "tooltip")
                        .attr("x", xPosition)
                        .attr("y", yPosition)
                        .attr("class", "barLabel")
                        .text(d.toFixed(2));
               })
               .attr("fill", function(d) {
                    return "rgb(0,0," + cScale(d) + ")";
                })
                .on("mouseout", function(d) {
                    d3.select(this)
                        .transition()
                       .duration(300)
                       .attr("fill", "rgb(0,0," + cScale(d) + ")");

                    d3.select("#tooltip").remove();

                    d3.select("#tooltipDiv").classed("hidden", true);
                });
                //.append("title")
                //.text(function(d) {
                //    return d.toFixed(3);
                //});

// make labels for svg bars
let labels = svgB.selectAll("text")
            .data(randomDataset)
            .enter()
            .append("text")
            .text(function(d) {
                return d.toFixed(1);
            })
            .attr("x", function(d, i) {
                return xScale(i) + xScale.bandwidth() / 2;
            })
            .attr("y", function(d, i) {
                return yScale(d);
            })
            .attr("class", "barLabel")
            .style("pointer-events", "none");

// create y-axis for plot
var yAxis = d3.axisLeft()
              .scale(yScale); // TODO
                
// create group for axis and position it
svgB.append("g")
    .attr("class", "axis")
    .attr("transform", "translate(" + (xScale.range()[0] - 3) + ",0)")
    .call(yAxis);


d3.select("#updateBar")
    .on("click",  function() {
        randomDataset = generateRandom(numBars, maxVal, minVal);

        svgB.selectAll("rect")
            .data(randomDataset)
            .transition()
            .delay(100)
            .duration(500)
            .ease(d3.easeCubicInOut)
            .attr("x", function(d, i) {
                return xScale(i);
            })
            .attr("y", function(d) {
                return yScale(d);
            })
            .attr("height", function(d) {
                return yScale.range()[0] - yScale(d);
           })
           .attr("fill", function(d) {
              return "rgb(0,0," + cScale(d) + ")";
           });

        svgB.selectAll(".barLabel")
            .data(randomDataset)
            .transition()
            .delay(100)
            .duration(500)
            .text(function(d) {
                return d.toFixed(1);
            })
            .attr("x", function(d, i) {
                return xScale(i) + xScale.bandwidth() / 2;
            })
            .attr("y", function(d, i) {
                return yScale(d);
            });

        yScale.domain([0, d3.max(randomDataset)]).nice();
        cScale.domain([d3.min(randomDataset),
                        d3.max(randomDataset)]);

        svgB.select(".axis")
            .transition()
            .duration(500)
            .call(yAxis);



    });

    d3.select("#addBar")
        .on("click", function() {
            var newNumber = Math.floor(Math.random() * maxVal);

            randomDataset.push(newNumber);
            numBars++;

            xScale.domain(d3.range(randomDataset.length));
            var bars = svgB.selectAll("rect")
                            .data(randomDataset);

            bars.enter()
                .append("rect")
               .attr("y", function(d) {
                    return yScale(d);
               })
               .attr("x", w)
               .attr("width", xScale.bandwidth)
               .attr("height", function(d) {
                    return yScale.range()[0] - yScale(d);
               })
                .attr("fill", function(d) {
                    return "rgb(0,0," + cScale(d) + ")";
                })
                .attr("width", xScale.bandwidth())
                .merge(bars)
                .transition()
                .delay(100)
                .duration(500)
                .ease(d3.easeCubicInOut)
                .attr("x", function(d, i) {
                    return xScale(i);
                })
                .attr("y", function(d) {
                    return yScale(d);
                })
                .attr("height", function(d) {
                    return yScale.range()[0] - yScale(d);
                })
                .attr("fill", function(d) {
                    return "rgb(0,0," + cScale(d) + ")";
                });
                
            

        svgB.selectAll("text")
            .data(randomDataset)
            .transition()
            .delay(100)
            .duration(500)
            .text(function(d) {
                return d.toFixed(1);
            })
            .attr("x", function(d, i) {
                return xScale(i) + xScale.bandwidth() / 2;
            })
            .attr("y", function(d, i) {
                return yScale(d);
            });

        yScale.domain([0, d3.max(randomDataset)]).nice();
        cScale.domain([d3.min(randomDataset),
                        d3.max(randomDataset)]);

        svgB.select(".axis")
            .transition()
            .duration(500)
            .call(yAxis);

    });


    d3.select("#removeBar")
         .on("click", function() {
        
        randomDataset.pop();
        numBars--;

        xScale.domain(d3.range(randomDataset.length));
        var bars = svgB.selectAll("rect")
            .data(randomDataset);

        bars.exit()
            .transition()
            .duration(500)
            .attr("x", w)
            .remove();
            
        bars.transition()
            .delay(100)
            .duration(500)
            .attr("x", function(d, i) {
                return xScale(i);
            })
            .attr("y", function(d) {
                return yScale(d);
            })
            .attr("width", xScale.bandwidth)
            .attr("height", function(d) {
                return yScale.range()[0] - yScale(d);
            })
            .attr("fill", function(d) {
                return "rgb(0,0," + cScale(d) + ")";
            });
        
        
        var labels = svgB.selectAll("text")
            .data(randomDataset);

        labels
            .transition()
            .delay(100)
            .duration(500)
            .attr("x", function(d, i) {
                return xScale(i) + xScale.bandwidth() / 2;
            })
            .attr("y", function(d, i) {
                return yScale(d);
            });

        labels.exit()
            .transition()
            .duration(500)
            .attr("x", w)
            .remove();

        yScale.domain([0, d3.max(randomDataset)]).nice();
        cScale.domain([d3.min(randomDataset),
                        d3.max(randomDataset)]);

        svgB.select(".axis")
            .transition()
            .duration(500)
            .call(yAxis);
        
    })


}