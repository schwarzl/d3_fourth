{
var w = 600;
let h = 250;
let padding = 30;

// define datasets
let maxVal = 100;
let randomDataset = [];

function generateRandomXY(num) {
  randomDataset = [];
  for (let i = 0; i < num; i++) {
    randomDataset.push({ x: Math.round(Math.random() * maxVal),
                         y: Math.round(Math.random() * maxVal) });
  }
};

generateRandomXY(20); // populate randomDataset

var xScale = d3.scaleLinear()
  .domain([0, d3.max(randomDataset, function(randomDataset) {return randomDataset.x;})])
  .rangeRound([padding, w - padding])
  .nice(5);

var yScale = d3.scaleLinear()
  .domain([0, d3.max(randomDataset, function(randomDataset) {return randomDataset.x;})])
  .range([h - padding, padding])
  .nice(5);

// svgS will hold our bar plot
let svgS = d3.select("#svgScatter")
  .append("svg")
  .attr("viewBox", "0, 0, " + w + ", " + h);
  // SVG internal coordinate system is defined in viewBox
  // can be used to calculate component sizes

svgS.attr("width", "100%");
  // to allow scaling width is set to 100% - relative to the enclosing div
  // height is not set (default is auto) to keep the proper aspect ratio
  // apparently setting it to "auto" from D3 doesn't work, it wants a number

  //Define clipping path
  svgS.append("clipPath")
    .attr("id", "chart-area")
    .append("rect")
  //Make a new clipPath
  //Assign an ID
  //Within the clipPath, create a new rect
    .attr("x", padding) //Set rect's position and size...
    .attr("y", padding)
    .attr("width", w - padding * 2)
    .attr("height", h - padding * 2);

let circles = svgS.append("g")
  .attr("id", "circles")
  .attr("clip-path", "url(#chart-area)")
  .selectAll("circle")
  .data(randomDataset)
  .enter()
  .append("circle")
  .attr("cx", function(d){
    return xScale(d.x);
})
  .attr("cy", function(d){
    return yScale(d.y);
})
  .attr("r", function(d){
    return Math.round(d.y/10);
  })
  .attr("fill", function(d){
    return "rgb(0,0," + Math.round(d.x*255/maxVal) + ")";
  })
  .attr("class", "scatterpoint");

  var xAxis = d3.axisBottom()
                .scale(xScale);

  var yAxis = d3.axisLeft()
                .scale(yScale);

  svgS.append("g")
      .attr("transform", "translate(0,"+yScale.range()[0]+")")
      .attr("class", "xaxis")
      .call(xAxis);

  svgS.append('g')
      .attr('class', 'yaxis')
      .attr('transform', 'translate(' + xScale.range()[0] + ',0)')
      .call(yAxis);

  d3.select("button")
    .on("click", function() {
        console.log("Button clicked");
        generateRandomXY(100);
      
      
    });
}
